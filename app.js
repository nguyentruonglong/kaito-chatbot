'use strict';

const apiai = require('apiai');
const express = require('express');
const bodyParser = require('body-parser');
const uuid = require('uuid');
const request = require('request');
const JSONbig = require('json-bigint');
const async1 = require('asyncawait/async');
const await = require('asyncawait/await');
const async = require('async');
var util = require('./util.js');
var MongoClient = require("mongodb").MongoClient;
//var urlDB = 'mongodb://127.0.0.1:27017/LongDB';
var urlDB = 'mongodb://nguyentruonglong:123456789@ds129023.mlab.com:29023/kaitochatbot';

const REST_PORT = (process.env.PORT || 5000);
const APIAI_ACCESS_TOKEN = '601f427b00574093b541bbc8c2887db5';
const APIAI_LANG = 'en';
const FB_VERIFY_TOKEN = '123456789!@#$%';
const FB_PAGE_ACCESS_TOKEN = 'EAAcPKWRTC6MBAKAlqDGRMZCw4FtfBv2wQkLnu3Gem7jIk7ByR3q3p1wUlOlzge9lWxPpk1XaaBV9VR5IFaVRA7cMep1aDuJn1M6nUZC7BB03TxUjqZBv3VlFAiqaNXjEV9nbKh9hPZBTK90RYzK7ZCKovStakCtbKizCBZChqZBtDYM8tqoPdKe';
const FB_TEXT_LIMIT = 640;

const FACEBOOK_LOCATION = "FACEBOOK_LOCATION";
const FACEBOOK_WELCOME = "FACEBOOK_WELCOME";
const FB_GRAPH_URL = 'https://graph.facebook.com/v2.8/USER_ID?fields=first_name,last_name&access_token=';
class FacebookBot {
    constructor() {
        this.apiAiService = apiai(APIAI_ACCESS_TOKEN, {language: APIAI_LANG, requestSource: "fb"});
        this.sessionIds = new Map();
        this.messagesDelay = 200;
    }


    doDataResponse(sender, facebookResponseData) {
        if (!Array.isArray(facebookResponseData)) {
            console.log('Response as formatted message');
            this.sendFBMessage(sender, facebookResponseData)
                .catch(err => console.error(err));
        } else {
            async.eachSeries(facebookResponseData, (facebookMessage, callback) => {
                if (facebookMessage.sender_action) {
                    console.log('Response as sender action');
                    this.sendFBSenderAction(sender, facebookMessage.sender_action)
                        .then(() => callback())
                        .catch(err => callback(err));
                }
                else {
                    console.log('Response as formatted message');
                    this.sendFBMessage(sender, facebookMessage)
                        .then(() => callback())
                        .catch(err => callback(err));
                }
            }, (err) => {
                if (err) {
                    console.error(err);
                } else {
                    console.log('Data response completed');
                }
            });
        }
    }

    doRichContentResponse(sender, messages) {
        let facebookMessages = []; // array with result messages
            for (let messageIndex = 0; messageIndex < messages.length; messageIndex++) {
            let message = messages[messageIndex];
            switch (message.type) {

                //message.type 0 means text message
                case 0:
                    // speech: ["hi"]
                    // we have to get value from fulfillment.speech, because of here is raw speech
                    if (message.speech) {

                        let splittedText = this.splitResponse(message.speech);

                        splittedText.forEach(s => {
                            facebookMessages.push({text: s});
                        });
                    }

                    break;
                //message.type 1 means card message
                case 1: {
                    let carousel = [message];

                    for (messageIndex++; messageIndex < messages.length; messageIndex++) {
                        if (messages[messageIndex].type == 1) {
                            carousel.push(messages[messageIndex]);
                        } else {
                            messageIndex--;
                            break;
                        }
                    }

                    let facebookMessage = {};
                    carousel.forEach((c) => {
                        // buttons: [ {text: "hi", postback: "postback"} ], imageUrl: "", title: "", subtitle: ""

                        let card = {};

                        card.title = c.title;
                        card.image_url = c.imageUrl;
                        if (this.isDefined(c.subtitle)) {
                            card.subtitle = c.subtitle;
                        }
                        //If button is involved in.
                        if (c.buttons.length > 0) {
                            let buttons = [];
                            for (let buttonIndex = 0; buttonIndex < c.buttons.length; buttonIndex++) {
                                let button = c.buttons[buttonIndex];

                                if (button.text) {
                                    let postback = button.postback;
                                    if (!postback) {
                                        postback = button.text;
                                    }

                                    let buttonDescription = {
                                        title: button.text
                                    };

                                    if (postback.startsWith("http")) {
                                        buttonDescription.type = "web_url";
                                        buttonDescription.url = postback;
                                    } else {
                                        buttonDescription.type = "postback";
                                        buttonDescription.payload = postback;
                                    }

                                    buttons.push(buttonDescription);
                                }
                            }

                            if (buttons.length > 0) {
                                card.buttons = buttons;
                            }
                        }

                        if (!facebookMessage.attachment) {
                            facebookMessage.attachment = {type: "template"};
                        }

                        if (!facebookMessage.attachment.payload) {
                            facebookMessage.attachment.payload = {template_type: "generic", elements: []};
                        }

                        facebookMessage.attachment.payload.elements.push(card);
                    });

                    facebookMessages.push(facebookMessage);
                }

                    break;
                //message.type 2 means quick replies message
                case 2: {
                    if (message.replies && message.replies.length > 0) {
                        let facebookMessage = {};

                        facebookMessage.text = message.title ? message.title : 'Choose an item';
                        facebookMessage.quick_replies = [];

                        message.replies.forEach((r) => {
                            facebookMessage.quick_replies.push({
                                content_type: "text",
                                title: r,
                                payload: r
                            });
                        });

                        facebookMessages.push(facebookMessage);
                    }
                }

                    break;
                //message.type 3 means image message
                case 3:

                    if (message.imageUrl) {
                        let facebookMessage = {};

                        // "imageUrl": "http://example.com/image.jpg"
                        facebookMessage.attachment = {type: "image"};
                        facebookMessage.attachment.payload = {url: message.imageUrl};

                        facebookMessages.push(facebookMessage);
                    }

                    break;
                //message.type 4 means custom payload message
                case 4:
                    if (message.payload && message.payload.facebook) {
                        facebookMessages.push(message.payload.facebook);
                    }
                    break;

                default:
                    break;
            }
        }

        return new Promise((resolve, reject) => {
            async.eachSeries(facebookMessages, (msg, callback) => {
                    this.sendFBSenderAction(sender, "typing_on")
                        .then(() => this.sleep(this.messagesDelay))
                        .then(() => this.sendFBMessage(sender, msg))
                        .then(() => callback())
                        .catch(callback);
                },
                (err) => {
                    if (err) {
                        console.error(err);
                        reject(err);
                    } else {
                        console.log('Messages sent');
                        resolve();
                    }
                });
        });

    }

    doTextResponse(sender, responseText) {
        console.log('Response as text message');
        // facebook API limit for text length is 640,
        // so we must split message if needed
        let splittedText = this.splitResponse(responseText);

        async.eachSeries(splittedText, (textPart, callback) => {
            this.sendFBMessage(sender, {text: textPart})
                .then(() => callback())
                .catch(err => callback(err));
        });
    }

    //which webhook event
    getEventText(event) {
        if (event.message) {
            if (event.message.quick_reply && event.message.quick_reply.payload) {
                return event.message.quick_reply.payload;
            }

            if (event.message.text) {
                return event.message.text;
            }
        }

        if (event.postback && event.postback.payload) {
            return event.postback.payload;
        }

        return null;

    }

    getFacebookEvent(event) {
        if (event.postback && event.postback.payload) {

            let payload = event.postback.payload;

            switch (payload) {
                case FACEBOOK_WELCOME:
                    return {name: FACEBOOK_WELCOME};

                case FACEBOOK_LOCATION:
                    return {name: FACEBOOK_LOCATION, data: event.postback.data}
            }
        }

        return null;
    }

    processFacebookEvent(event) {
        const sender = event.sender.id.toString();
        const eventObject = this.getFacebookEvent(event);
        if (eventObject) {

            // Handle a text message from this sender
            if (!this.sessionIds.has(sender)) {
                this.sessionIds.set(sender, uuid.v4());
            }
            let apiaiRequest = this.apiAiService.eventRequest(eventObject,
                {
                    sessionId: this.sessionIds.get(sender),
                    originalRequest: {
                        data: event,
                        source: "facebook"
                    }
                });

            this.doApiAiRequest(apiaiRequest, sender);
        }
    }

    processMessageEvent(event) {
      this.tmp_event = event;
        const sender = event.sender.id.toString();
        const text = this.getEventText(event);

        if (text) {
            // Handle a text message from this sender
            if (!this.sessionIds.has(sender)) {
                this.sessionIds.set(sender, uuid.v4());
            }

            console.log("Text", text);
            //send user's text to api.ai service
            let apiaiRequest = this.apiAiService.textRequest(text,
                {
                    sessionId: this.sessionIds.get(sender),
                    originalRequest: {
                        data: event,
                        source: "facebook"
                    }
                });

            console.log("===============Api request==================");
            console.log(apiaiRequest);
            console.log("============================================");

            this.doApiAiRequest(apiaiRequest, sender);
        }
    }

    doApiAiRequestSmalltalk(apiaiRequestSmalltalk, sender) {
        apiaiRequestSmalltalk.on('response', (response) => {
            if (this.isDefined(response.result) && this.isDefined(response.result.fulfillment)) {
                let responseText = response.result.fulfillment.speech;
                let responseData = response.result.fulfillment.data;
                let responseMessages = response.result.fulfillment.messages;
                console.log("===============API response====================");
                console.log(JSON.parse(JSON.stringify(response)));
                console.log("===============================================");
                console.log("===============Api contexts==================");
                console.log(JSON.parse(JSON.stringify(response.result.contexts)));
                console.log("============================================");

                if (this.isDefined(responseData) && this.isDefined(responseData.facebook)) {
                    let facebookResponseData = responseData.facebook;
                    this.doDataResponse(sender, facebookResponseData);
                } else if (this.isDefined(responseMessages) && responseMessages.length > 0) {
                    this.doRichContentResponse(sender, responseMessages);
                }
            }
        });
        apiaiRequestSmalltalk.on('error', (error) => console.error(error));
        apiaiRequestSmalltalk.end();

    }

    doApiAiRequest(apiaiRequest, sender) {
        apiaiRequest.on('response', (response) => {
            if (this.isDefined(response.result) && this.isDefined(response.result.fulfillment)) {
                let responseText = response.result.fulfillment.speech;
                let responseData = response.result.fulfillment.data;
                let responseMessages = response.result.fulfillment.messages;
                let text = response.result.resolvedQuery;

                console.log("===============API response====================");
                console.log(JSON.parse(JSON.stringify(response)));
                console.log("===============================================");
                console.log("===============Api contexts==================");
                console.log(JSON.parse(JSON.stringify(response.result.contexts)));
                console.log("============================================");

                if (this.isDefined(responseData) && this.isDefined(responseData.facebook)) {
                    let facebookResponseData = responseData.facebook;
                    this.doDataResponse(sender, facebookResponseData);
                } else if (this.isDefined(responseMessages) && responseMessages.length > 0) {
                    this.doRichContentResponse(sender, responseMessages);
                }
                else if (this.isDefined(responseText)) {
                    this.doTextResponse(sender, responseText);
                }
                else {
                  var apiaiRequestSmalltalk = this.apiAiService.textRequest(text,
                  {
                      sessionId: this.sessionIds.get(sender),
                      contexts:[
                          {
                            name: "smalltalk",
                            lifespan: 1
                          }
                        ],
                        originalRequest: {
                            source: "facebook"
                        }
                  });
                  this.doApiAiRequestSmalltalk(apiaiRequestSmalltalk, sender);
                }
            }
        });

        apiaiRequest.on('error', (error) => console.error(error));
        apiaiRequest.end();
    }

    splitResponse(str) {
        if (str.length <= FB_TEXT_LIMIT) {
            return [str];
        }

        return this.chunkString(str, FB_TEXT_LIMIT);
    }

    chunkString(s, len) {
        let curr = len, prev = 0;

        let output = [];

        while (s[curr]) {
            if (s[curr++] == ' ') {
                output.push(s.substring(prev, curr));
                prev = curr;
                curr += len;
            }
            else {
                let currReverse = curr;
                do {
                    if (s.substring(currReverse - 1, currReverse) == ' ') {
                        output.push(s.substring(prev, currReverse));
                        prev = currReverse;
                        curr = currReverse + len;
                        break;
                    }
                    currReverse--;
                } while (currReverse > prev)
            }
        }
        output.push(s.substr(prev));
        return output;
    }

    sendFBMessage(sender, messageData) {
        return new Promise((resolve, reject) => {
            request({
                url: 'https://graph.facebook.com/v2.6/me/messages',
                qs: {access_token: FB_PAGE_ACCESS_TOKEN},
                method: 'POST',
                json: {
                    recipient: {id: sender},
                    message: messageData
                }
            }, (error, response) => {
                if (error) {
                    console.log('Error sending message: ', error);
                    reject(error);
                } else if (response.body.error) {
                    console.log('Error: ', response.body.error);
                    reject(new Error(response.body.error));
                }

                resolve();
            });
        });
    }

    sendFBSenderAction(sender, action) {
        return new Promise((resolve, reject) => {
            request({
                url: 'https://graph.facebook.com/v2.6/me/messages',
                qs: {access_token: FB_PAGE_ACCESS_TOKEN},
                method: 'POST',
                json: {
                    recipient: {id: sender},
                    sender_action: action
                }
            }, (error, response) => {
                if (error) {
                    console.error('Error sending action: ', error);
                    reject(error);
                } else if (response.body.error) {
                    console.error('Error: ', response.body.error);
                    reject(new Error(response.body.error));
                }

                resolve();
            });
        });
    }

    doSubscribeRequest() {
        request({
                method: 'POST',
                uri: `https://graph.facebook.com/v2.6/me/subscribed_apps?access_token=${FB_PAGE_ACCESS_TOKEN}`
            },
            (error, response, body) => {
                if (error) {
                    console.error('Error while subscription: ', error);
                } else {
                    console.log('Subscription result: ', response.body);
                }
            });
    }

    configureGetStartedEvent() {
        request({
                method: 'POST',
                uri: `https://graph.facebook.com/v2.6/me/thread_settings?access_token=${FB_PAGE_ACCESS_TOKEN}`,
                json: {
                    setting_type: "call_to_actions",
                    thread_state: "new_thread",
                    call_to_actions: [
                        {
                            payload: FACEBOOK_WELCOME
                        }
                    ]
                }
            },
            (error, response, body) => {
                if (error) {
                    console.error('Error while subscription', error);
                } else {
                    console.log('Subscription result', response.body);
                }
            });
    }

    getInformation(userID) {
      var graph_link = `https://graph.facebook.com/v2.8/USER_ID?fields=first_name,last_name&access_token=`
      var url_graph = graph_link.replace("USER_ID", userID);
      console.log(url_graph);
      return new Promise((resolve, reject) => {
        request({
          method: 'GET',
          uri: url_graph + FB_PAGE_ACCESS_TOKEN
        },
          (error, response, body) => {
          if (error) {
            console.error('Error while Get Information User : ', error);
            reject(error);
          } else {
            console.log('Information result: ', response.body);
            resolve(JSON.parse(response.body).last_name);
          }
        });
      });
    }

    isDefined(obj) {
        if (typeof obj == 'undefined') {
            return false;
        }

        if (!obj) {
            return false;
        }

        return obj != null;
    }

    sleep(delay) {
        return new Promise((resolve, reject) => {
            setTimeout(() => resolve(), delay);
        });
    }

}


let facebookBot = new FacebookBot();

const app = express();

app.use(bodyParser.text({type: 'application/json'}));

app.get('/', (req, res) => {
    if (req.query['hub.verify_token'] === FB_VERIFY_TOKEN) {
        res.send(req.query['hub.challenge']);

        setTimeout(() => {
            facebookBot.doSubscribeRequest();
        }, 3000);
    } else {
        res.send('Error, wrong validation token');
    }
});

var stt = 0;
var answer_arr = [];

app.post('/webhook', (req, res) => {
   const data = JSONbig.parse(req.body);
   var action = String(data.result.action);
   var speech = data.result.resolvedQuery;
   let id = data.originalRequest.data.sender.id;
   console.log(id+"zzzzzzzzzzzzz");
     var info = facebookBot.getInformation(id);
   console.log(action);
   switch(action) {

     // Tách Email và Password từ input nếu đúng ở định dạng (Email/Password) và so sánh với dữ liệu trong Collection AdminAcccount
     case "checkUsernamePassword":
       var userpass = speech;
       // Lấy giá trị vị trí của ký tự '/' trong chuỗi nhập vào
       var index = userpass.indexOf("/");
       if (index < 1 || index >= userpass.length-1){
         // Nếu vị trí ký tự '/' ở đầu chuỗi hoặc cuối chuỗi thì gọi Event nhập sai định dạng cho API.AI
         util.sendJson(res, "UsernamePasswordInvalidEvent", null, null, null);
       }
       else {
           // Nếu đúng định dạng thì tách Email và Password
         var splitEmail = userpass.substring(0, index);
         var splitPassword = userpass.substring(index+1, userpass.length);
         // Gọi hàm kiểm tra định dạng Email bằng Regex
         util.checkValidEmail(splitEmail)
         .then(check => {
           if(check){
             util.getEmail(splitEmail)
             .then(result =>{
               // Nếu định dạng Email là hợp lệ
               if(result == splitEmail){
                 // Gọi hàm so sánh Email và Password với dữ liệu trong Collection AdminAccount
                 util.checkLogin(result, splitPassword)
                 .then (resCheck => {
                     if(resCheck){
                       // Nếu khớp với dữ liệu thì gọi Event UsernamePasswordValidEvent
                       util.sendJson(res, "UsernamePasswordValidEvent", null, null, null);
                     }
                     else {
                       // Gọi Event UsernamePasswordInvalidEvent nếu không khớp với dữ liệu trong Database
                       util.sendJson(res, "UsernamePasswordInvalidEvent", null, null, null);
                     }
                   }, err => console.log(err+"")
                 );
               }
               else {
                  // Gọi Event UsernamePasswordInvalidEvent nếu nhập sai hoặc không hợp lệ
                  util.sendJson(res, "UsernamePasswordInvalidEvent", null, null, null);
               }
             })
           }
           else {
             // Gọi Event UsernamePasswordInvalidEvent nếu nhập sai hoặc không hợp lệ
             util.sendJson(res, "UsernamePasswordInvalidEvent", null, null, null);
           }
         })
       }

      break;

    // Nhận tên Survey do Admin nhập vào và khởi tạo tham số gồm danh sách câu hỏi và danh sách thể loại câu trả lời tương ứng
    case "InputSurveyName":
      // Khởi tạo tham số cho Context ListQuestion gồm tên Survey, danh sách câu hỏi và danh sách thể loại câu trả lời tương ứng
      var list_question_answer = {
        "survey_name": speech,
        "questions": [],
        "answerType": []
      }
      // Gọi Event InputQuestionEvent để nhập câu hỏi đồng thời truyền tham số vào Context ListQuestion
      util.sendJson(res, "InputQuestionEvent", null, "listquestion", list_question_answer);
      break;

    // Nhận câu hỏi do Admin nhập vào và lưu nó vào tham số trong Context ListQuestion
    case "MakeQuestion":
      // Tìm Context listquestion trong các Contexts
      var surveyObj = data.result.contexts.filter(function (e) {
        return e.name == "listquestion";
      });
      // Thêm câu hỏi do Admin nhập vào biến questions
      surveyObj[0].parameters.questions.push(speech);
      var addQuestion = surveyObj[0].parameters.questions;
      // Cập nhật lại biến question trong tham số của Context ListQuestion
      var list_question_answer
       = {
        "survey_name": surveyObj[0].parameters.survey_name ,
        "questions": addQuestion,
        "answerType": surveyObj[0].parameters.answerType
      }
      // Gọi Event InputAnswerTypeEvent và truyền tham số thông qua ContextOut để lưu lại các câu hỏi đã nhập
      util.sendJson(res, "InputAnswerTypeEvent", null, "listquestion", list_question_answer);
      break;

    // Lấy danh sách loại câu trả lời từ Database và tạo Carousel các lựa chọn tương ứng
    case "InputAnswerType":
    case "TypeAnswerInvalid":
      // Gọi hàm lấy danh sách loại câu trả lời từ Database
      util.getTypeAnswer()
        .then(result => {
          // Tùy vào Action mà đưa ra phản hồi tương ứng
          var chooseTypeAnswer = "Which type of answer would you like to use:\n";
          var responseTypeAnswerInvalid = "Incorrect answer type. Please input only:\n";
          // Tạo Carousel
          var listTypeAnswer = [];
            for(var i = 0; i < result.length; i++){
               var tmp  =
               {
                 "title": (i+1) + ". " + String(result[i].answer).replace(/,/g, "/"),
                 "image_url": util.getImageURL(i),
                 "buttons": [
                   {
                     "type": "postback",
                     "title": "Pick this",
                     "payload": result[i].type
                   }
                 ]
               }
              listTypeAnswer.push(tmp);
            }
            // Tạo phản hồi tùy vào Action
            var messageQR = {};
            if(action == "InputAnswerType") {
              messageQR = {
                  "data":{
                      "facebook": [
                        {
                        "text": chooseTypeAnswer
                        },
                        {
                          "attachment": {
                            "type": "template",
                            "payload": {
                             "template_type": "generic",
                              "elements": listTypeAnswer
                            }
                          }
                        }
                      ]
                    }
                }
            }
            else  {
              messageQR = {
                "data":{
                    "facebook": [
                      {
                      "text": responseTypeAnswerInvalid
                      },
                      {
                        "attachment": {
                          "type": "template",
                          "payload": {
                           "template_type": "generic",
                            "elements": listTypeAnswer
                          }
                        }
                      }
                    ]
                  }
                }
            }
            // Gửi phản hồi chứa biến Json đã tạo đến API.AI
            res.status(200).json({
              source: 'webhook',
              data: messageQR.data
            });
          }, err => console.log(err+''));

          break;

    // Nhận loại câu trả lời do Admin nhập vào và lưu nó vào tham số trong Context ListQuestion
    case "GetTypeAnswer":
      // Gọi hàm lấy danh sách loại câu trả lời từ Database
      util.getTypeAnswer()
      .then(result => {
        // Gọi hàm kiểm tra tính hợp lệ của thể loại câu trả lời được Admin nhập
        util.checkTypeAnswer(result, speech)
        .then(check_result => {
          if(check_result) {
            // Tìm Context listquestion trong các Contexts
            var surveyObj = data.result.contexts.filter(function (e) {
              return e.name == "listquestion";
            });
            // Thêm thể loại câu trả lời do Admin nhập vào biến answerType
            surveyObj[0].parameters.answerType.push(speech);
            var addAnswerType = surveyObj[0].parameters.answerType;
            // Cập nhật lại biến answerType trong tham số của Context ListQuestion
            var list_question_answer = {
              "survey_name": surveyObj[0].parameters.survey_name ,
              "questions": surveyObj[0].parameters.questions ,
              "answerType": addAnswerType
            }
              // Gọi Event TypeAnswerValidEvent và truyền tham số thông qua ContextOut để lưu lại thể loại câu trả lời đã nhập
              util.sendJson(res, "TypeAnswerValidEvent", null, "listquestion", list_question_answer);
          }
          else {
              // Gọi Event TypeAnswerInvalidEvent nếu kết quả kiểm tra loại câu trả lời là không hợp lệ
              util.sendJson(res, "TypeAnswerInvalidEvent", null, null, null);
          }
        })
      })
      break;

    // Gọi lại Event InputQuestionEvent nếu Admin muốn nhập thêm câu hỏi
    // Gọi Event NoMoreQuestionsEvent nếu Admin kết thúc tạo survey và lưu thông tin survey vào Database
    // Gọi Event InvalidSelectionEvent nếu nhập không đúng định dạng hợp lệ
    case "AnotherQuestion":

      if( speech.toLowerCase() == "yes") {
        // Nếu Admin nhấn Yes thì gọi Event InputQuestionEvent để nhập thêm câu hỏi
        util.sendJson(res, "InputQuestionEvent", null, null, null);
      }
      // Nếu Admin nhấn No
      else if ( speech.toLowerCase() == "no") {
        // Tìm Context listquestion trong các Contexts
        var surveyObj = data.result.contexts.filter(function (e) {
          return e.name == "listquestion";
        });
        // Lấy tên survey đã lưu trong tham số ở Context ListQuestion
        var name = surveyObj[0].parameters.survey_name;
        // Lấy danh sách câu hỏi đã lưu trong tham số ở Context ListQuestion
        var arr_question = surveyObj[0].parameters.questions;
        // Lấy danh sách thể loại câu trả lời đã lưu trong tham số ở Context ListQuestion
        var arr_answer_type = surveyObj[0].parameters.answerType;
        // Lấy độ dài của danh sách câu hỏi và thể loại câu trả lời
        var len = surveyObj[0].parameters.questions.length;

        // Tạo biến Json để lưu thông tin survey vào Database
        var databaseObj = {
            "survey_name" : name,
            "survey_question_list" : []
        };

        for (var i = 0; i < len; i++) {
          databaseObj.survey_question_list.push({
                    "survey_question" : arr_question[i],
                    "answer_type" : arr_answer_type[i]
                });
        }
        // Gọi hàm để lưu thông tin survey vào Database
        util.saveSurveyName(name, databaseObj);
        // Lấy danh sách câu hỏi tạo thành phản hồi cho Admin
        var responseListQuestion = "So here is list of your questions: \n";
        for (var i = 1; i <= len; i++) {
          responseListQuestion = responseListQuestion + "Question "+i+": "+arr_question[i-1]+ "\n";
        }

        var tmp = {
          "questions": responseListQuestion ,
          "message" : "Your "+surveyObj[0].parameters.survey_name+" survey is successfully created. Now you can try that by saying \"Hello\"."
        };
        // Gọi Event NoMoreQuestionsEvent để API.AI gửi phản hồi chứa danh sách câu hỏi cho Admin
        util.sendJson(res, "NoMoreQuestionsEvent", tmp, null, null);

      }
      else {
        // Gọi Event InvalidSelectionEvent nếu Admin không nhập Yes hoặc No
        util.sendJson(res, "InvalidSelectionEvent", null, null, null);
      }
      break;




      //====== khi User nói "hello" thì bot sẽ truy xuất ID của User để lấy tên và chào lại ============
        case "User-Hello": {

                info.then(function (result) {

                    return res.status(200).json({
                        "followupEvent": {
                            "name": "choose_survey",
                            "data": {
                                "name": result
                            }
                        }
                    });

                });
            }
            break;
            //==============================================================================================


            //======= sau khi Bot chào lại, nếu trong database không có Survey nào thì yêu cầu tạo survey, còn ngược lại thì sẽ nhảy qua Event hỏi chọn Survey =============
        case "User-Hello.User-make_survey": {

                MongoClient.connect(urlDB, function (err, db) { ///////// kết nối database từ localhost:27017
                    if (err)
                        throw err;
                    facebookBot.doRichContentResponse(id, data.result.fulfillment.messages)
                    db.collection("SurveyDB").find().toArray(function (err, result) { /////////tìm dữ liệu trong collection SurveyDB
                        if (err)
                            throw err;
                        if (result[0].survey_list == "") { ////////nếu không có survey nào được tìm thấy thì sẽ nhảy qua Event yêu cầu tạo survey
                            return res.status(200).json({
                                "followupEvent": {
                                    "name": "NoSurvey"
                                }
                            })
                        } else {
                  setTimeout(function(){
                            return res.status(200).json({ ///////nếu có survey được tìm thấy thì nhảy qua Event hỏi chọn survey
                                "followupEvent": {
                                    "name": "AskSurvey"
                                }
                            })
                },2000);
                        }
                    })
                })
            }
            break;
            //=======================================================================================================================================================


            //============hỏi chọn Survey và bắt đầu tải Carousel có các button là Survey lên =========================
        case "Hello.make_survey.User-AskSurvey": {

                var ask = async1(function () {
                        await(facebookBot.doRichContentResponse(id, data.result.fulfillment.messages)) ///hỏi chọn survey
                        await(util.load_survey(res)); ///// tải Carousel có các button là Survey lên
                    })
                    ask().catch (function (err) {
                        console.log(err)
                    })

            }
            break;
            //========================================================================================================


            //================= khi không có survey thì Bot sẽ xin lỗi và yêu cầu tạo Survey nếu User đó là Admin=========
        case "User-Hello.User-make_survey.User-NoSurvey": {}
            break;
            //========================================================================================================


            //=================sau khi User chọn xong Survey thì Bot sẽ tiến hành từ Survey đó và sẽ tải lên câu hỏi đầu tiên của Survey từ database, nếu User nhập sai Survey thì nó sẽ nhảy qua Event xuất lỗi=========
        case "Hello.make_survey.User-AskSurvey.User-AskSurvey-Class": {
                MongoClient.connect(urlDB, function (err, db) { ///////// kết nối database từ localhost:27017
                    if (err)
                        throw err;
                    db.collection("SurveyDB").find().toArray(function (err, result) { /////////tìm dữ liệu trong collection SurveyDB
                        if (err)
                            throw err;
                        var arr = [];
                        var val = 0;
                        var name_survey = data.result.resolvedQuery;
                        for (var i = 0; i < result[0].survey_list.length; i++) {
                            if (result[0].survey_list[i].survey_name == name_survey) { ///////tìm survey tương ứng trong database
                                var wait = async1(function () {
                                        await(facebookBot.doRichContentResponse(id, data.result.fulfillment.messages)) /////sau khi User chọn survey thì Bot sẽ xác nhận Survey đó
                                        await(util.show_question(res, name_survey, 0, arr)); ////// Bot sẽ tải câu hỏi đầu tiên của Survey đó từ database
                                    })
                                    wait().catch (function (err) {
                                        console.log(err)
                                    })

                                        break;
                            }
                            val = val + 1;
                        }
                        if (val == result[0].survey_list.length) { ///// nếu tên Survey mà User nhập vào không khớp với database thì sẽ nhảy qua Event xuất ra lỗi
                            return res.status(200).json({
                                "followupEvent": {
                                    "name": "failSurvey"
                                }
                            });
                        }
                    });
                });
            }
            break;
            //=============================================================================================================================================================================================================


            //=========xuất ra lỗi khi Survey mà User nhập không chính xác và quay trở lại Event hỏi chọn Survey=======================
        case "Hello.make_survey.User-AskSurvey.User-AskSurvey-Class.User-ErrorSurvey": {
                var wait = async1(function () {
                        await(facebookBot.doRichContentResponse(id, data.result.fulfillment.messages)) ////xuất ra lỗi khi Survey mà User nhập không chính xác
                        await(res.status(200).json({ //////nhảy lại Event hỏi chọn Survey
                                "contextOut": [{
                                        "name": "User-make_survey-followup",
                                        "lifespan": 2,
                                    }
                                ],
                                "followupEvent": {
                                    "name": "AskSurvey"
                                }
                            }));
                    })
                    wait().catch (function (err) {
                        console.log(err)
                    })
            }
            break;
            //======================================================================================================================


            //==========sau khi xuất câu hỏi đầu tiên thì đồng thời tìm trong database và xuất ra Quick replies chứa câu trả lời tương ứng với câu hỏi====================
        case "Hello.make_survey.User-AskSurvey.User-AskSurvey-Class.User-AskFirstQuestion": {
                for (var i = 0; i < data.result.contexts.length; i++) {
                    if (data.result.contexts[i].name == "class") {
                        var b = data.result.contexts[i].parameters.any; ////tìm tên Survey được chọn
                    }
                }
                var First_Q = async1(function () {
                        await(util.answer_rep(res, b, data.result.parameters.FirstQuestion.speech, 0, data.result.fulfillment.messages[0].speech, data.result.parameters.wrong_answer)) ///xuất ra Quick replies chứa câu trả lời tương ứng với câu hỏi
                    })
                    First_Q().catch (function (err) {
                        console.log(err)
                    })
            }
            break;
            //============================================================================================================================================================


            //=============sau khi User trả lời câu hỏi đầu tiên thì Bot sẽ tiếp tục tìm trong database để xuất ra câu hỏi tiếp theo===================================================================
        case "Hello.make_survey.User-AskSurvey.User-AskSurvey-Class.User-AskFirstQuestion.User-Next": {
                var first;
                var nexts;
                var texts = data.result.fulfillment.speech; /// chứa nội dung câu nói của Bot



        for (var i = 0; i < data.result.contexts.length; i++) {
            if (data.result.contexts[i].name == "class") {
                var survey = data.result.contexts[i].parameters.any; //////// chứa tên survey mà user đã chọn
                first = data.result.contexts[i].parameters.FirstQuestion //////// chứa câu hỏi đầu tiên theo survey
                    var stt = data.result.contexts[i].parameters.stt; /////// biến đếm thứ tự câu hỏi
                var answer_arr = data.result.contexts[i].parameters.answer_arr; ///// mảng lưu trữ câu trả lời
                nexts = data.result.contexts[i].parameters.NextQuestion; /////// chứa câu hỏi tiếp theo survey
            }
        }
        MongoClient.connect(urlDB, function (err, db) {
            if (err)
                throw err;
            db.collection("SurveyDB").find().toArray(function (err, result) { ////// tìm dữ liệu trong database SurveyDB
                if (err)
                    throw err;
                var arr_button = [];
                var array_list = [];
                var name_survey;
                var answer_type;
                var user_answer1 = data.result.resolvedQuery; ///////// chứa câu trả lời của user
            var user_answer = user_answer1.toLowerCase();

                for (let i = 0; i < result[0].survey_list.length; i++) {
                    if (result[0].survey_list[i].survey_name == survey) { ///////////// tìm survey tương ứng với survey mà user đã chọn
                        for (let j = 0; j < result[0].survey_list[i].survey_question_list.length; j++) { //////// chạy hết list câu hỏi trog survey đó
                            name_survey = result[0].survey_list[i].survey_name;
                            var question_survey = result[0].survey_list[i].survey_question_list[j].survey_question;
                            array_list[j] = question_survey; ///// lưu trữ list câu hỏi
                        }
                    }
                }
                for (var i = 0; i < result[0].survey_list.length; i++) {
                    if (result[0].survey_list[i].survey_name == survey) { ///tìm survey tương ứng với survey mà user đã chọn
                        answer_type = result[0].survey_list[i].survey_question_list[stt].answer_type; ///// chọn ra loại câu trả lời tương ứng theo biến đếm thứ tự câu hỏi stt
                        break;
                    }
                }
                for (var j = 0; j < result[0].answer_type_list.length; j++) {
                    if (result[0].answer_type_list[j].type == answer_type) { //////////////thông qua loại câu trả lời tra nội dung của loại câu trả lời trong doccument answer_type_list của database
                        var valu = 0;
                        for (var k = 0; k < result[0].answer_type_list[j].answer.length; k++) {
                            if (user_answer == result[0].answer_type_list[j].answer[k].toLowerCase()) { ////////// tìm trong list nội dung của loại trả lời xem có tương ứng với câu trả lời của user hay không
                                break
                            }
                            valu = valu + 1;
                        }
                        if (valu == result[0].answer_type_list[j].answer.length) { //////nếu không có câu trả lời tương ứng
                            if (stt == 0) { ///// và tại câu hỏi thứ nhất thì xuất ra câu nói của Bot
                                return res.status(200).json({
                                    "contextOut": [{
                                            "name": "User-AskSurvey-Class-followup",
                                            "lifespan": 2,
                                        }
                                    ],
                                    "followupEvent": {
                                        "name": "FirstQ",
                                        "data": {
                                            "speech": texts,
                                            "FirstQuestion": first
                                        }
                                    }
                                });
                            }
                  var stt1= String(stt+1);
                            if (stt > 0) { ///// và tại câu hỏi tiếp theo thì xuất ra câu nói của Bot
                                return res.status(200).json({
                                    "contextOut": [{
                                            "name": "User-Next-followup",
                                            "lifespan": 2,
                                        }
                                    ],
                                    "followupEvent": {
                                        "name": "NextQ",
                                        "data": {
                                            "speech": texts,
                                            "NextQuestion": nexts,
                          "Stt1":stt1

                                        }
                                    }
                                });
                            }
                        }
                        break;
                    }
                }
                var posfinal = array_list.length; //// số lượng câu hỏi
                answer_arr.push(user_answer); /// đẩy câu trả lời của user sau khi đã kiểm tra có tồn tại giống với loại câu trả lời vào mảng câu trả lời
                stt = stt + 1;
                if (stt == posfinal) { /////// nếu đã hết câu hỏi thì chuyển sang nội dung tiếp theo
                    var insert_answer = util.insert_db(id, survey, answer_arr, null); ///// thực hiện lưu trữ câu trả lời vào database khi chưa có suggest
                    info.then(function (result) {
                        return res.status(200).json({ /// nhảy qua Event yêu cầu suggest
                            "followupEvent": {
                                "name": "suggest",
                                "data": {
                                    "name": result,
                                    "any1": name_survey
                                }
                            }
                        });
                    });
                }
                if (stt < posfinal) { /////// nếu chưa hết câu hỏi thì tiếp tục show câu tiếp theo
                    var Next_Q = async1(function () {
                            await(util.show_question(res, survey, stt, answer_arr));
                        })
                        Next_Q().catch (function (err) {
                            console.log(err)
                        })
                }
            });
        });

      }
        break;
        //============================================================================================================================================================================================



        //==========sau khi xuất câu hỏi tiếp theo thì đồng thời tìm trong database và xuất ra Quick replies chứa câu trả lời tương ứng với câu hỏi====================
      case "Hello.make_survey.User-AskSurvey.User-AskSurvey-Class.User-AskFirstQuestion.User-Next.User-AskNextQuestion": {
        for (var i = 0; i < data.result.contexts.length; i++) {
            if (data.result.contexts[i].name == "class") {
                var b = data.result.contexts[i].parameters.any; ////tìm tên Survey được chọn
                var stt = data.result.contexts[i].parameters.stt; ////tìm số thứ tự câu hỏi
            }
        }
        var Answer_next = async1(function () {
                await(util.answer_rep(res, b, data.result.parameters.NextQuestion.speech, stt, data.result.fulfillment.messages[0].speech, data.result.parameters.wrong_answer1)) ////xuất ra Quick replies chứa câu trả lời tương ứng với câu hỏi
            })
            Answer_next().catch (function (err) {
                console.log(err)
            })
      }
        break;
        //===============================================================================================================================================================


        //============lưu suggest của User vào database AnswerDB==============================================================================
      case "User-suggest.User-give-suggest": {
        var survey;
        for (var i = 0; i < data.result.contexts.length; i++) {
            if (data.result.contexts[i].name == "class") {
                survey = data.result.contexts[i].parameters.any; //// tìm tên survey mà user chọn
            }
        }
        var insert_suggest = util.insert_db(id, survey, null, data.result.resolvedQuery); /// thực hiện lưu suggest và không cần lưu câu trả lời
        var wait = async1(function () {
                await(facebookBot.doRichContentResponse(id, data.result.fulfillment.messages)) /// chuyển sang nội dung tiếp theo
                await(res.status(200).json({
                        "followupEvent": {
                            "name": "help"
                        }
                    }));
            })
            wait().catch (function (err) {
                console.log(err)
            })

      }
        break;
        //===================================================================================================================================


        //========sau khi User hoàn thành Survey thì Bot sẽ hỏi lựa chọn tiếp tục hay kết thúc, và khi User chọn "Take another survey" thì sẽ nhảy về Event hỏi chọn lại Survey=========
      case "User-suggest.User-suggest-custom.User-give-suggest-custom.User-make_another_survey": {
        var wait = async1(function () {
                await(facebookBot.doRichContentResponse(id, data.result.fulfillment.messages))
                await(res.status(200).json({ ///////nhảy về Event hỏi chọn lại Survey
                        "followupEvent": {
                            "name": "AskSurvey"
                        }
                    }));
            })
            wait().catch (function (err) {
                console.log(err)
            })
      }
        break;
        //==============================================================================================================================================================================


        //=====nếu User nhập không chính xác với lựa chọn Bot đưa ra thì sẽ xuất ra lỗi và Bot sẽ yêu cầu lựa chọn lại======================
      case "User-suggest.User-suggest-custom.User-give-suggest-custom.User-Help-fallback": {

        var wait = async1(function () {
                await(facebookBot.doRichContentResponse(id, data.result.fulfillment.messages)) //////xuất ra lỗi khi User nhập lựa chọn không chính xác
                await(res.status(200).json({ ////////nhảy về Event yêu cầu lựa chọn tiếp tục hay kết thúc
                        "followupEvent": {
                            "name": "help"
                        }
                    }));
            })
            wait().catch (function (err) {
                console.log(err)
            })
      }
        break;
    default:
      break;
   }
 });

app.post('/', (req, res) => {
    try {
        const data = JSONbig.parse(req.body);

        if (data.entry) {
            let entries = data.entry;
            entries.forEach((entry) => {
                let messaging_events = entry.messaging;
                if (messaging_events) {
                    messaging_events.forEach((event) => {
                        if (event.message && !event.message.is_echo) {
                            if (event.message.attachments) {
                                let locations = event.message.attachments.filter(a => a.type === "location");

                                // delete all locations from original message
                                event.message.attachments = event.message.attachments.filter(a => a.type !== "location");

                                if (locations.length > 0) {
                                    locations.forEach(l => {
                                        let locationEvent = {
                                            sender: event.sender,
                                            postback: {
                                                payload: "FACEBOOK_LOCATION",
                                                data: l.payload.coordinates
                                            }
                                        };

                                        facebookBot.processFacebookEvent(locationEvent);
                                    });
                                }
                            }
                            facebookBot.processMessageEvent(event);
                        } else if (event.postback && event.postback.payload) {
                            if (event.postback.payload === "FACEBOOK_WELCOME") {
                                facebookBot.processFacebookEvent(event);
                            } else {
                                facebookBot.processMessageEvent(event);
                            }
                        }
                    });
                }
            });
        }

        return res.status(200).json({
            status: "ok"
        });
    } catch (err) {
        return res.status(400).json({
            status: "error",
            error: err
        });
    }

});

app.listen(REST_PORT, () => {
    util.addPersistentMenu();
    console.log('Rest service ready on port ' + REST_PORT);
});

facebookBot.doSubscribeRequest();