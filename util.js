const request = require('request');
const REST_PORT = (process.env.PORT || 5000);
const APIAI_ACCESS_TOKEN = '601f427b00574093b541bbc8c2887db5';
const APIAI_LANG = 'en';
const FB_VERIFY_TOKEN = '123456789!@#$%';
const FB_PAGE_ACCESS_TOKEN = 'EAAcPKWRTC6MBAKAlqDGRMZCw4FtfBv2wQkLnu3Gem7jIk7ByR3q3p1wUlOlzge9lWxPpk1XaaBV9VR5IFaVRA7cMep1aDuJn1M6nUZC7BB03TxUjqZBv3VlFAiqaNXjEV9nbKh9hPZBTK90RYzK7ZCKovStakCtbKizCBZChqZBtDYM8tqoPdKe';
const FB_TEXT_LIMIT = 640;
const async1 = require('asyncawait/async');
const await = require('asyncawait/await');
const async = require('async');

var MongoClient = require("mongodb").MongoClient;
var ObjectId = require('mongodb').ObjectID;
var urlDB = 'mongodb://nguyentruonglong:123456789@ds129023.mlab.com:29023/kaitochatbot';
var surveyDBName = 'SurveyDB';

// Tạo Persistent Menu bằng JSON
function addPersistentMenu(){
	request({
		method: 'POST',
		uri: `https://graph.facebook.com/v2.6/me/thread_settings?access_token=${FB_PAGE_ACCESS_TOKEN}`,
		json:{
			setting_type : "call_to_actions",
			thread_state : "existing_thread",
			call_to_actions:[
				{
				  type:"postback",
				  title:"Take survey",
				  payload: "take survey"
				},
				{
				  type:"postback",
				  title:"Create Survey",
				  payload: "Create Survey"
				},
				{
				  type:"postback",
				  title:"Help",
				  payload:"Help"
				}
			]
		}},
		(error, response, body) => {
				if (error) {
					console.error('Error while subscription', error);
				} else {
					console.log('Subscription result', response.body);
				}
		}
	);
}

module.exports.addPersistentMenu = addPersistentMenu;

// Xóa Persistent Menu bằng JSON
function removePersistentMenu(){
	request({
		method: 'POST',
		uri: `https://graph.facebook.com/v2.6/me/thread_settings?access_token=${FB_PAGE_ACCESS_TOKEN}`,
		json:{
			setting_type : "call_to_actions",
			thread_state : "existing_thread",
			call_to_actions:[]
		}},
		(error, response, body) => {
				if (error) {
					console.error('Error while subscription', error);
				} else {
					console.log('Subscription result', response.body);
				}
		}
	);
}

module.exports.removePersistentMenu = removePersistentMenu;

// Xử lý đoạn Json và gửi đoạn Json đó cho API.AI
var sendJson = (res, eventName, parametersEvent, contextName, parametersContext) => {
  var tmp = null;
	// Tạo biến Json chứa các tham số của ContextOut và FollowupEvent dựa vào các tham số truyền vào của hàm
  if(contextName != null) {
    tmp = {
        "contextOut": [{"name": contextName, "lifespan":1, "parameters":parametersContext}],
        "followupEvent" :{
            "name": eventName
        }
      };
  }
  else {
    tmp = {
        "followupEvent" :{
            "name": eventName,
        }
      };
  }

  if(parametersEvent != null)
  {
    tmp.followupEvent.data = parametersEvent;
  }

	// Gửi biến chứa Json đã xử lý đến API.AI
  return res.status(200).json(tmp);
}

module.exports.sendJson = sendJson;

// Kiểm tra định dạng của chuỗi có chứa Email hay không bằng Regex
var checkValidEmail = (input) => {
  return new Promise((resolve, reject) => {
    var regex = /(?!(\. | \n | \s))+([a-zA-Z0-9])+([a-zA-Z0-9]|(\.))*@((([a-zA-Z0-9])+(\.))+([a-zA-Z0-9])+)(?!(\. | \n | \s))+/;
    resolve(regex.test(input));
  });
};

module.exports.checkValidEmail = checkValidEmail;

// Tách ra Email từ chuỗi có chứa Email bằng Regex
var getEmail = (input) => {
  return new Promise((resolve, reject) => {
    var regex = /(?!(\. | \n | \s))+([a-zA-Z0-9])+([a-zA-Z0-9]|(\.))*@((([a-zA-Z0-9])+(\.))+([a-zA-Z0-9])+)(?!(\. | \n | \s))+/;
    resolve(regex.exec(input)[0]);
  });
};

module.exports.getEmail = getEmail;

// Kiểm tra username và password với Database
var checkLogin = (username, password) => {
  return new Promise((resolve, reject) => {
    MongoClient.connect(urlDB, function(err,db){
      if(err) {
        throw err;
      } else {
        var collection = db.collection('AdminAccount');
				// Tìm username và password trong Collection AdminAccount tương ứng với tham số truyền vào và trả về kết quả ở dạng array
        collection.find({"admin_list.username": username, "admin_list.password": password}).toArray(function (err, result) {
          if (err) {
						// Nếu xảy ra lỗi
            return reject(new Error('Error!'));
          } else if (result.length) {
						// Nếu array trả về có số phần tử lớn hơn 0 thì trả kết quả về true
            db.close();
            resolve(true);
          } else {
						// Nếu array trả về có số phần tử bằng 0 thì trả kết quả về false
            db.close();
            console.log('No ID found!');
            resolve(false);
          }
        });
      };
    });
  });
}

module.exports.checkLogin = checkLogin;

// Lấy URL ảnh cho carousel
var getImageURL = (input) => {
  var imgURL = "";
  switch(input % 6) {
      case 0:
          imgURL = "http://pijarnews.com/wp-content/uploads/2017/03/survey.jpg";
          break;
      case 1:
          imgURL = "https://www.superb.net/blog/wp-content/uploads/survey.jpg";
          break;
      case 2:
          imgURL = "https://www.gailperry.com/wp-content/uploads/2014/04/survey1.jpg";
          break;
      case 3:
          imgURL = "http://www.mayersmemorial.com/pictures/content/122253.jpg";
          break;
      case 4:
          imgURL = "http://www.stupidsid.com/images/papersolutions/icons/fbShare3.png";
          break;
      default:
          imgURL = "http://akcdn.okccdn.com/media/img/icons/home_new_user_guide.png";
  }
  return imgURL;
};

module.exports.getImageURL = getImageURL;

// Lấy các thể loại câu trả lời từ Database
var getTypeAnswer = () => {
  return new Promise((resolve, reject) => {
    MongoClient.connect(urlDB, function(err,db){
      if(err) {
        throw err;
      } else {
        var collection = db.collection(surveyDBName);
				// Lấy danh sách loại câu trả lời trong Collection SurveyDB
        collection.find({}).toArray(function (err, result) {
          if (err) {
            return reject(new Error('Error!'));
          } else if (result.length) {
            console.log(result);
            db.close();
            resolve(result[0].answer_type_list);
          } else {
            db.close();
            console.log('No ID found!');
            resolve(false);
          }
        });
      };
    });
  });
}

module.exports.getTypeAnswer = getTypeAnswer;

// Kiểm tra thể loại câu trả lời nhập vào với Database
var checkTypeAnswer = (input, speech) => {
  return new Promise((resolve, reject) => {
    for(var i = 0; i<input.length; i++) {
			// So sánh dữ liệu nhập vào với các phần tử trong danh sách thể loại câu trả lời
     if(input[i].type == speech)
        resolve(true);
    }
    resolve(false);
  });
}

module.exports.checkTypeAnswer = checkTypeAnswer;

// Lưu thông tin survey vào Database
var saveSurveyName = (name, databaseObj) => {
  MongoClient.connect(urlDB, function(err,db){
    if(err) {
      throw err;
    } else {
      var collection = db.collection(surveyDBName);
			// Tìm Object có survey_name tương ứng với tên Survey trong mảng survey_list
      collection.find({"survey_list.survey_name": name}).toArray(function (err, result) {
        if (err) {
        }
				// Nếu mảng survey_list có tồn tại Object với survey_name tương ứng (array trả về có độ dài lớn hơn 0)
				else if (result.length) {
          console.log(result);
					// Xóa Object trong mảng survey_list với survey_name tương ứng khỏi Collection SurveyDB
          collection.update(
            { _id : ObjectId("597fec7af36d280abc42b426")},
            { $pull: { survey_list: { "survey_name" : name} } }
          );
        }
        console.log(result);
				// Thêm Object chứa thông tin survey mới vào mảng survey_list trong Collection SurveyDB
        collection.update( { _id : ObjectId("597fec7af36d280abc42b426") },{ $push: { survey_list: databaseObj } });
        db.close();
        console.log('No ID found!');
      });
    }
  });
}

module.exports.saveSurveyName = saveSurveyName;




//////////////////////////////////////////////// tải survey từ database lên api.ai ////////////////////////////////////////////////////


var load_survey = function (res) {
	MongoClient.connect(urlDB, function (err, db) {                            /////// kết nối database tới localhost:27017
		if (err)
			throw err;

		db.collection("SurveyDB").find().toArray(function (err, result) {			/////////////  thực hiện tìm kiếm dữ liệu từ collection SurveyDB

			if (err)
				throw err;
			var arr_button = [];

			for (var i = 0; i < result[0].survey_list.length; i++) {				/////////////  tìm tên survey
				var name_survey = result[0].survey_list[i].survey_name;
				arr_button[i] = { 													//////////// gán tên survey vào các button của Carousel card
					title: "🔎"+name_survey,

					image_url: "https://lh4.ggpht.com/-KN8lBjB7f2g/WRqMYTbGj8I/AAAAAAAALaU/6sprwn8df7Qg9Syg9p1tTqtXw5DVrpm1ACLcB/s1600/Survey.png",

					buttons: [{
							"type": "postback",
							"title":  "Select" ,
							"payload": name_survey
						}
					]
				};

			}
			var test = {													////////// xuất ra Carousel với các button là tên survey
				facebook: {
					attachment: {
						type: "template",
						payload: {
							template_type: "generic",
							elements: arr_button
						}
					}
				}
			};
			// the most basic response
			res.status(200).json({									//////////////  gửi Carousel lên api.ai với nguồn là /webhook
				source: 'webhook',

				data: test,

			})

		});

	});
}
module.exports.load_survey = load_survey;



//////////////////////////////// tải các câu hỏi của 1 survey lên và chuyển câu hỏi vào các Event phù hợp ///////////////////////


var show_question = function (res, survey, stt, answer_arr) {

	MongoClient.connect(urlDB, function (err, db) {										 /////// kết nối database tới localhost:27017
		if (err)
			throw err;

		db.collection("SurveyDB").find().toArray(function (err, result) {				/////////////  thực hiện tìm kiếm dữ liệu từ collection SurveyDB

			if (err)
				throw err;
			var arr_button = [];

			for (var i = 0; i < result[0].survey_list.length; i++) {
				var name_survey = result[0].survey_list[i].survey_name;														///////// tìm tên survey từ database
				var question_survey = result[0].survey_list[i].survey_question_list[stt].survey_question;					///////// truy xuất ra các câu hỏi từ survey được tìm thấy

        var stt1 = stt+1;
				var ques = question_survey;
				if (name_survey == survey) {									/////////// nếu tên survey trùng với survey được tìm thấy trong database thì thực hiện lấy câu hỏi từ survey đó
					var a = {
						'speech': ques
					};
					break;
				}
			}

			if (stt === 0) {													/////////// nếu là câu hỏi đầu tiên thì sẽ đưa dữ liệu là câu hỏi đầu tiên trong database qua Event của nó

				return res.status(200).json({
					"contextOut": [{"name":"class", "lifespan":2, "parameters":{"stt":0,"answer_arr":[]}}],
					"followupEvent": {
						"name": "FirstQ",
						"data": {
							"FirstQuestion": a
						}
					}
				});
			} else {

				return res.status(200).json({									////////// nếu là những câu hỏi tiếp theo thì sẽ đưa dữ liệu là câu hỏi trong database qua Event của nó
					"contextOut": [{"name":"class", "lifespan":2, "parameters":{"stt":stt,"answer_arr":answer_arr}}],
					"followupEvent": {
						"name": "NextQ",
						"data": {
							"NextQuestion": a,
							"Stt1": String(stt1)
						}

					}
				});
			}

		});
	});
}
module.exports.show_question = show_question;




//////////////////////////////// gán các câu trả lời thành Quick replies và tải các câu trả lời cho câu hỏi tương ứng lên api.ai /////////////////////////////////
var answer_rep = function (res, survey, question, stt, messages, texts) {
	MongoClient.connect(urlDB, function (err, db) {																						/////// kết nối database tới localhost:27017
		if (err)
			throw err;
		var answer_type;

		db.collection("SurveyDB").find().toArray(function (err, result) {																/////////////  thực hiện tìm kiếm dữ liệu từ collection SurveyDB

			if (err)
				throw err;
			var arr_button = [];

			for (var i = 0; i < result[0].survey_list.length; i++) {

				if ((result[0].survey_list[i].survey_name == survey) && (result[0].survey_list[i].survey_question_list[stt].survey_question == question)) {
					var answer_type_question = result[0].survey_list[i].survey_question_list[stt].answer_type;							///////// tìm loại câu trả lời từ database nếu tên survey và câu hỏi của survey trùng với dữ liệu trong database tương ứng

					for (var j = 0; j < result[0].answer_type_list.length; j++) {

						if (result[0].answer_type_list[j].type == answer_type_question) {
							for (var k = 0; k < result[0].answer_type_list[j].answer.length; k++) {
								var buttons = result[0].answer_type_list[j].answer[k];											////// tìm các câu trả lời từ database tương ứng với loại của nó

								let button = {								///////////  gán các câu trả lời vào button của Quick replies
									"content_type": "text",
									"title": buttons,
									"payload": buttons
								}
								arr_button.push(button);
							}
						}

					}

				}

			}

			if (texts == "") {												 ////////// xuất ra Quick replies với các button là các câu trả lời tương ứng với từng câu hỏi của một survey

				var test = {
					facebook: [{
							"text": messages,
							"quick_replies": arr_button
						}
					]
				};
			} else if (texts != "") {                              ///////////////// xuất ra Quick replies với các button là các câu trả lời tương ứng với từng câu hỏi của một survey kèm theo 1 đoạn text trước nó
				var test = {
					facebook: [{
							"text": texts
						}, {
							"text": messages,
							"quick_replies": arr_button
						}
					]
				};
			}

			//facebookBot.doRichContentResponse(id, data.result.fulfillment.messages);
			// the most basic response
			res.status(200).json({
				source: 'webhook',
				data: test,
			});
		});
	});
}
module.exports.answer_rep = answer_rep;



var insert_db = function (id, survey, answer, suggest) {

	MongoClient.connect(urlDB, function (err, db) {
		if (err)
			throw err;
		db.collection("answerdb").find().toArray(function (err, result) { ///// mở database answerdb

			if (err)
				throw err;
			var val = 0;

			for (var j = 0; j < result.length; j++) {

				if (result[j].id == id) {             ///////////////////  dò tìm id của user tương ứng trong database

					var value = 0;
					for (var i = 0; i < result.length; i++) {
						if (result[i].survey == survey) { //////////////// dò tìm survey tương ứng trong database theo id

							if (suggest != null) { ///////////////// Dò tìm xem có suggest của user hay không
								var json = {
									"suggest": suggest
								}
								db.collection("answerdb").findOneAndUpdate({ /// cập nhật lại suggest của user
									id: id,
									survey: survey
								}, {
									$set: json
								}, {
									upsert: true
								}, function (err, doc) {
									if (err) {
										console.log(err);
									}
								})
							} else {
								var json = {
									"answer": answer
								}
								db.collection("answerdb").findOneAndUpdate({////// nếu dữ liệu đưa vào không có suggest thì cập nhật lại answer
									id: id,
									survey: survey
								}, {
									$set: json
								}, {
									upsert: true
								}, function (err, doc) {
									if (err) {
										console.log(err);
									}
								})

							}
							break;
						}

						value = value + 1;
					}
					if (value == result.length) {   /////// nếu chưa có survey nhưng đã có id thì thêm survey mới với cấu trúc tương ứng

						db.collection("answerdb").insertOne({

							"id": id,
							"survey": survey,
							"answer": answer,
							"suggest": suggest

						}, function (err, result) {

						});

					}
					break;
				}

				val = val + 1;
			}

			if (val == result.length) {    ///////////////// sau khi dò tìm không có và không thấy id tương ứng


				db.collection("answerdb").insertOne({       ///////////// lưu vào database nội dung của user với cấu trúc answer, suggest, id, survey tương ứng
					"id": id,
					"survey": survey,
					"answer": answer,
					"suggest": suggest
				}, function (err, result) {

				});
			}
		});
	});
}
module.exports.insert_db = insert_db;